var expect = require('chai').expect;
var request = require('request');

var app = require('../app.js');
var server = null;

describe('server response', function () {
  before(function () {
    server = app.listen(3000);
  });

  it('should return 200', function (done) {
    request.get('http://localhost:3000', function (err, res, body){
      expect(res.statusCode).to.equal(200);
      expect(res.body).to.equal('Hello NodeJS!');
      done();
    });
  });

  after(function () {
    server.close();
  });
});